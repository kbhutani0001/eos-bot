import os
import time
import re
from slackclient import SlackClient
import config
from datetime import datetime
from profanity import profanity
from textblob import TextBlob as tb
# instantiate Slack client
slack_client = SlackClient(config.token)
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

def sentiment_analysis(msg):
    analysis = tb(msg)
    result = analysis.sentiment.polarity
    if result>0.5:
        return "grinning"
    elif result<-0.5:
        return "no_mouth"
    elif result<0.2 and result>-0.2:
        return "slightly_smiling_face"
    else:
        return None
def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        print(slack_events,"\n\n")
        #greetings on channel join
        if event["type"]=="member_joined_channel":
            user_info=slack_client.api_call("users.info",user=event["user"])
            if user_info["user"]["real_name"]:
                name=user_info["user"]["real_name"]
            else:
                name="@" + user_info["user"]["name"]
            slack_client.api_call(
            "chat.postMessage",
            channel=event["channel"],
            text="Welcome to EOS {} :)\nPlease introduce yourself.".format(name)
            )
        if event["type"] == "message" and not "subtype" in event:
            if(profanity.contains_profanity(event["text"])):
                user_info=slack_client.api_call("users.info",user=event["user"])
                if user_info["user"]["real_name"]:
                    name=user_info["user"]["real_name"]
                else:
                    name="@" + user_info["user"]["name"]
                slack_client.api_call(
                "chat.postMessage",
                channel=event["channel"],
                text="{} Please mind your language!".format(name)
                )
            user_id, message = parse_direct_mention(event["text"])
            return message, event["channel"]
    return None, None

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the use

    # Finds and executes the given command, filling in response
    response = None
    # This is where you start to implement more commands!
    if command.startswith("do"):
        response = "Sure...write some more code then I can do that!"

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=response
    )
    if "sad" in command:
        with open('bunny.png', 'rb') as file_content:
            slack_client.api_call("files.upload",channels=channel,file=file_content,title="Ughh :(")
    if ("gsoc" in command) and ("wiki" in command):
        slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text="https://gitlab.com/SUSE-UIUX/eos/wikis/GSoC-2019-Sub-org-at-Python.org:x  -EOS"
        )
    if ("remind" in command):
        #split message on spaces
        msgs=command.split(" ")
        if "/" in msgs[1]:
            date=msgs[1]
            time=msgs[2]
        else:
            date=msgs[2]
            time=msgs[1]
        dates=date.split("/")
        times=time.split(":")
        times[0]=int(times[0])
        times[1]=int(times[1])
        if(times[1]<30):
            times[0]-=1
            times[1]+=30
        else:
            times[1]-=30
        dt=datetime(int(dates[0]),int(dates[1]),int(dates[2]),times[0],times[1])
        timestamp = datetime.timestamp(dt)
        slack_client.api_call(
        "chat.scheduleMessage",
        channel=channel,
        post_at=timestamp,
        text="Meetup in 30 minutes! Get ready everyone :)"
        )
        slack_client.api_call(
        "reminders.add",
        text="Meetup in 30 minutes! Get ready everyone :)",
        post_at=timestamp
        )
        slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text="Message Schdeuled"
        )


    if ("git" in command) and ("wiki" in command):
        slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text="https://gitlab.com/SUSE-UIUX/eos/wikis/GSoC-basic-git-instructions"
        )
# constants
RTM_READ_DELAY = 0.1 # 1 second delay between reading from RTM
EXAMPLE_COMMAND = "do"
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"
if __name__ == "__main__":
    if slack_client.rtm_connect(with_team_state=False):
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]
        while True:
            command, channel = parse_bot_commands(slack_client.rtm_read())
            if command:
                handle_command(command, channel)
            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")

