from trello import TrelloClient
import config

client = TrelloClient(
    api_key=config.trello_api,
    api_secret=config.trello_secret,
)
all_boards = client.list_boards()
last_board = all_boards[-1]
print(last_board.name)